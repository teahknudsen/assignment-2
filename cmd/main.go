package main

import (
	"assignment2"
	"cloud.google.com/go/firestore"
	"context"
	firebase "firebase.google.com/go"
	"fmt"
	"google.golang.org/api/option"
	"log"
	"net/http"
	"os"
)

var ctx context.Context
var client *firestore.Client

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Handler not implemented", http.StatusNotImplemented)
		return
	}
	_, err := fmt.Fprintf(w, "IMT2681 Assignment2 - Application with webhooks by Tea Høvik Knudsen")
	if err != nil {
		log.Printf("Something went wrong")
	}
}

func main() {
	// Firebase initialisation
	ctx = context.Background()
	sa := option.WithCredentialsFile("./imt2681-assignment2-83e74-firebase-adminsdk-5szfr-29082fa172.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Fatalln(err)
	}

	client, err = app.Firestore(ctx)
	if err != nil {
			log.Fatalln(err)
	}
	// Exporting to be used in database.go
	assignment2.DB.Client = client
	assignment2.DB.Ctx = ctx

	defer client.Close()

	port := "8080"

	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}

	http.HandleFunc("/repocheck/v1/", defaultHandler)
	http.HandleFunc("/repocheck/v1/status", assignment2.HandlerStatus)
	http.HandleFunc("/repocheck/v1/commits/", assignment2.HandlerCommit)
	http.HandleFunc("/repocheck/v1/languages", assignment2.HandlerLanguage)
	http.HandleFunc("/repocheck/v1/languages/", assignment2.HandlerLanguage)
	http.HandleFunc("/repocheck/v1/webhooks", assignment2.HandlerWebhooks)
	http.HandleFunc("/repocheck/v1/webhooks/", assignment2.HandlerWebhooks)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}