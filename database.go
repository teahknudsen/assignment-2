package assignment2

import (
	"bytes"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
	"time"

	"cloud.google.com/go/firestore"
	"golang.org/x/net/context"
)

// Webhooks struct
type Webhooks struct {
	ID    string
	Event string    `json:"event"`
	URL   string    `json:"url"`
	Time  time.Time `json:"time"`
}

// FirestoreDatabase struct for all initializing of databaseinfo
type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}

// Struct with information about an event sent to a url stored in webhooks
type webhookInvoke struct {
	Event string `json:"event"`
	Params []string `json:"params"`
	Time time.Time `json:"time"`
}
// Only need one connection to firestore
var DB FirestoreDatabase

// Used when invoking webhooks, calling webhooks URLs.
// Loops for webhooks in firestore for events equal to event sent when calling the function. It then sends a notification to the URL with the correct event.
func invokeWebhooks(event string, params []string, currentTime time.Time) {
	iter := DB.Client.Collection("webhooks").Documents(DB.Ctx)
	for {
		var webHook Webhooks
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("Failed to iterate: %v", err)
		}
		err = doc.DataTo(&webHook)
		if err != nil {
			log.Printf("Failed to parse database response: %v", err)
		}
		if webHook.Event == event {
			requestWebhook := webhookInvoke{Event: event, Params: params, Time: time.Now()}
			requestBody, err := json.Marshal(requestWebhook)
			if err != nil {
				log.Printf("Failed to marshall webhook call: %v", err)
			}
			_, err = http.Post(webHook.URL, "application/json", bytes.NewBuffer(requestBody))
			if err != nil {
				log.Printf("Not able to send webhook call: %v", err)
			}
		}
	}
}

// Handles relevant http methods (here: GET, POST and DELETE)
func HandlerWebhooks(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		// Default id is blank, finds eventual new id in the url from user
		webhookId := ""
		webhookId = r.URL.Path[23:]

		// Initialize variables
		var iteration *firestore.DocumentIterator
		var webhook Webhooks
		var listWebhook []Webhooks

		//If not blank, look for spesific id, otherwise fetch all
		if webhookId != "" {
			iteration = DB.Client.Collection("webhooks").Where("ID", "==", webhookId).Documents(DB.Ctx)
		}else {
			iteration = DB.Client.Collection("webhooks").Documents(DB.Ctx)
		}

		// Iterate the documents found into a list
		for {
			doc, err := iteration.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				log.Fatalf("Failed to iterate: %v", err)
			}
			if doc != nil {
				err = doc.DataTo(&webhook)
				listWebhook = append(listWebhook, webhook)
			}
		}

		w.Header().Add("content-type", "application/json")
		err := json.NewEncoder(w).Encode(listWebhook)
		if err != nil {
			http.Error(w, "Failed to encode webhooks", http.StatusInternalServerError)
		}

		//
	case http.MethodPost:
		var payload Webhooks
		err := json.NewDecoder(r.Body).Decode(&payload)
		if err != nil {
			http.Error(w, "Fail", http.StatusBadRequest)
		}
		// Checks for valid type
		if !(payload.Event == "commits"|| payload.Event == "languages" || payload.Event == "status") {
			http.Error(w, "Not valid type, please use 'commits', 'languages', or 'status'.", http.StatusBadRequest)
			return
		}
		_ = Save(&payload, w)


	case http.MethodDelete:
		var deleteWebhook Webhooks

		webhookID := r.URL.Path[23:]  // Get what is after letter number 23 in the URL
		deleteWebhook.ID = webhookID

		_ = Delete(&deleteWebhook)

	default:
		_, _ = fmt.Print("Not valid method")
	}
}

// SendMessage reads a string from the body in plain-text and sends it to firebase to be registered as a message.
func Save(web *Webhooks, w http.ResponseWriter) error {
	ref := DB.Client.Collection("webhooks").NewDoc()
	web.ID = ref.ID
	web.Time = time.Now()
	_, err := ref.Set(DB.Ctx, web)
	if err != nil {
		http.Error(w, "ERROR saving student to Firestore DB", http.StatusBadRequest)
	}
	_, err = fmt.Fprint(w, web.ID)
	if err != nil {
		http.Error(w, "Fail", http.StatusBadRequest)
	}
	return nil
}

func Delete(w *Webhooks) error {
	_, err := DB.Client.Collection("webhooks").Doc(w.ID).Delete(DB.Ctx)
	if err != nil {
		// Handle any errors in an appropriate way, such as returning them.
		log.Printf("An error has occurred: %s", err)
	}
	return nil
}
