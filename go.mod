module assignment2

go 1.13

require (
	cloud.google.com/go/firestore v1.0.0
	cloud.google.com/go/storage v1.1.2 // indirect
	firebase.google.com/go v3.9.0+incompatible
	go.opencensus.io v0.22.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	google.golang.org/api v0.13.0
	google.golang.org/appengine v1.6.5 // indirect
)
