package assignment2

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

const Git = "https://git.gvk.idi.ntnu.no/api/v4/projects"

// Struct for status endpoint
type Status struct {
	GitLab int `json:"gitlab"`
	Database int `json:"database"`
	Uptime string `json:"uptime"`
	Version int64 `json:"version"`
}

// Timestamp of the start of the program
var startTime = time.Now()

func HandlerStatus (w http.ResponseWriter, r *http.Request) {
	// The https://git.gvk.idi.ntnu.no/api/v4/projects is most likely to always be up, therefore, if I get data, i know it is working
	GitResponse, err := http.Get(Git)
	if err != nil {
		log.Fatalln(err)
		return
	}

	// Assume it is ok
	dbStatus := 200
	// Doc("0") is reserved for status check
	_, err = DB.Client.Collection("webhooks").Doc("0").Get(DB.Ctx)
	if err != nil {
		// If we can not get to server for a unknown reason, I give it a Service Unavailable error.
		dbStatus = 503
	}

	GitStatus := GitResponse.StatusCode

	// Calculates uptime of program
	newTime := time.Now()
	t := int64(newTime.Sub(startTime) / time.Second)

	// Stores all data into a statusOut struct
	statusOut := Status{GitStatus,dbStatus, "v1", t}

	// Prints data to website
	err = json.NewEncoder(w).Encode(statusOut)
	if err != nil {
		log.Fatalln(err)
	}
}