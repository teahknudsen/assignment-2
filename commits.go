package assignment2

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Repositories struct {
	Repos []RepoInfo		`json:"repos"`
	Authentication bool `json:"auth"`
}

type RepoInfo struct {
	Repository string 	`json:"path_with_namespace"`
	Commits int 		`json:"commits"`
}

type Id struct {
	Id int `json:"id"`
}

// Commit handler
func HandlerCommit (w http.ResponseWriter, r *http.Request) {
	// Only accepts GET requests.
	if r.Method != http.MethodGet {
		http.Error(w, "Not implemented yet", http.StatusNotImplemented)
		return
	}

	// Checks if there is a authentication token sent in the URL
	authenticate := ""
	if r.URL.Query()["private_token"] != nil {
		customToken := r.URL.Query()["private_token"][0]
		authenticate = customToken
	}

	// Checks for the custom limit, default is set to 5 from the description in the assignment.
	limit := 5
	// Check to see if limit is in the URL
	if r.URL.Query()["limit"] != nil {
		customLimit := r.URL.Query()["limit"][0]
		customLimitInt, err := strconv.Atoi(customLimit)
		if err == nil {
			limit = customLimitInt
		}
	}

	// Fetches the parameters that is used, then invokes webhook with subscription to commits endpoint.
	params := []string{strconv.Itoa(limit)}
	invokeWebhooks("commits", params, time.Now())

	// Initializing struct variables
	var Results Repositories
	var repositoryInfo RepoInfo
	var repositoryId []Id

	commitsCollect := make(map[int]int) // Map for collection commits

	pageNumber := 1   // To be able to search trough all pages in the API, starts at page 1

	resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", authenticate))
	if err != nil {
		log.Fatalln(err)
	}

	// Checks if the request has succeeded.
	if resp.StatusCode == 200 {
		numberOfPages, err2 := strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		if err2 != nil {
			log.Fatalln(err2)
		}

		// As long as the previous page was full (had 100 entries), it will continue to loop.
		for i := 1; i <= numberOfPages; i++ {
			resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page=%d&private_token=%s", pageNumber, authenticate ))
			if err != nil {
				log.Fatalln(err)
			}
			_ = json.NewDecoder(resp.Body).Decode(&repositoryId)

			// For each repo, get the X-total value from the header and saves these away.
			for x := 0; x < len(repositoryId); x++ {
				id := repositoryId[x].Id
				resp, err2 := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d/repository/commits?private_token=%s", id, authenticate))
				if err2 != nil {
					log.Fatalln(err2)
				}

				numberOfCommits, _ := strconv.Atoi(resp.Header.Get("X-Total"))
				commitsCollect[id] = numberOfCommits
			}

			// On to next page
			pageNumber += 1
		}

		// Loops to find the highest commit found and stores it. Loops amount of times equal to limit specified by user.
		for i := 0; i < limit; i++ {
			var largestKey int
			var largestValue int
			for key, value := range commitsCollect {
				if value > largestValue {
					largestValue = value
					largestKey = key
				}
			}
			resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d?private_token=%s", largestKey, authenticate))
			if err != nil {
				log.Fatalln(err)
			}
			_ = json.NewDecoder(resp.Body).Decode(&repositoryInfo)

			repositoryInfo.Commits = largestValue

			// Append the highest found to the list of repos that is going to be returned to user.
			// Deletes the highest value to avoid duplicate in next loop.
			Results.Repos = append(Results.Repos, repositoryInfo)
			delete(commitsCollect, largestKey)

		}

		// Checks if there is a authentication token sent or not.
		if authenticate == "" {
			Results.Authentication = false
		} else {
			Results.Authentication = true
		}
		w.Header().Add("content-type", "application/json")
		_ = json.NewEncoder(w).Encode(Results)
	} else {
		_, _ = fmt.Fprintf(w, "Malformed request%q", html.EscapeString(r.URL.Path))
	}
}