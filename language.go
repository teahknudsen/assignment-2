package assignment2

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
	"time"
)

// Global json struct
type Language struct {
	Languages	[]string 	`json:"name"`
	Auth		bool		`json:"auth"`
}

type Rep struct {
	Id int `json:"id"`
}

// Language handler
func HandlerLanguage (w http.ResponseWriter, r *http.Request) {
	// Checks if the request is a GET request, error message if there is no GET request.
	if r.Method != http.MethodGet {
		http.Error(w, "Not implemented yet", http.StatusNotImplemented)
		return
	}

	// Checks if there is a authentication token sent in the URL
	authenticate := ""
	if r.URL.Query()["private_token"] != nil {
		customToken := r.URL.Query()["private_token"][0]
		authenticate = customToken
	}


	// Checks for the custom limit, default is set to 5 from the description in the assignment.
	limit := 5
	if r.URL.Query()["limit"] != nil {
		customLimit := r.URL.Query()["limit"][0]
		customLimitInt, err := strconv.Atoi(customLimit) // Converts to int from string
		if err == nil {
			limit = customLimitInt
		}
	}

	// Fetches the parameters that is used, then invokes webhook with subscription to language endpoint.
	params := []string{strconv.Itoa(limit)}
	invokeWebhooks("languages", params, time.Now())

	// Initializing struct variables
	var getID []Rep
	var Results Language

	pageNumber := 1    // To be able to search trough all pages in the API, starts at page 1

	languageRank := make(map[string]int)

	// Fetches body if it exists. Does not handle authentication token from body.
	var ids []int
	err := json.NewDecoder(r.Body).Decode(&ids)
	if err != nil {
		log.Print("Failed to decode language body or it is blank", err)
	}

	resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", authenticate))
	if err != nil {
		log.Fatalln(err)
	}

	// If the query went trough it finds the total number of pages for the request.
	if resp.StatusCode == 200 {
		numberOfPages, err2 := strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		if err2 != nil {
			log.Fatalln(err2)
		}

		// As long as the previous page was full (had 100 entries), it will continue to loop.
		for z := 1; z <= numberOfPages; z++ {
			resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page=%d&private_token=%s", pageNumber, authenticate))
			if err != nil {
				log.Fatalln(err)
			}
			_ = json.NewDecoder(resp.Body).Decode(&getID)

			// Takes the language for each repository found in the first 100 and the following repositories.
			for x := 0; x < len(getID); x++ {
				id := getID[x].Id
				// Checks if the id is in the id's from body specified by user.
				// If it is found it breaks out and this id language is stored. If there is no body from the user, it will stay false.
				inIds := false
				for _, value := range ids {
					if id == value {
						inIds = true
						break
					}
				}

				// If there is no body from user or it was in the body from user, we save the language for the repository. If not, we go past it.
				if r.Body == http.NoBody || inIds {
					resp, err2 := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d/languages?private_token=%s", id, authenticate))
					if err2 != nil {
						log.Fatalln(err2)
					}

					// Map values that is found in language to string map.
					// Interface is not used, and therefore ignored.
					languages := make(map[string]interface{})
					err2 = json.NewDecoder(resp.Body).Decode(&languages)
					if err2 != nil {
						http.Error(w, "Error", http.StatusInternalServerError)
						return
					}

					// For each language in repository, the language occurrence increases.
					for key := range languages {
						languageRank[key]++
					}
				}
			}

			// On to next page
			pageNumber += 1
		}

		// Loops as many times as the amount of languages requested from the user.
		for i := 0; i < limit; i++ {
			var largestKey string
			var largestValue int
			for key, value := range languageRank {
				if value > largestValue {
					largestValue = value
					largestKey = key
				}
			}

			// Adding the highest value of language to results.
			// Deleted from the map. This is to avoid duplicates.
			if largestKey != "" {
			Results.Languages = append(Results.Languages, largestKey)
			delete(languageRank, largestKey)
			}
		}

		// Checks if there is a authentication token sent or not.
		if authenticate == "" {
			Results.Auth = false
		} else {
			Results.Auth = true
		}

		w.Header().Add("content-type", "application/json")
		_ = json.NewEncoder(w).Encode(Results)
	} else {
		// If status code is not 200 (OK)
		_, _ = fmt.Fprintf(w, "Malformed request%q", html.EscapeString(r.URL.Path))
	}
}