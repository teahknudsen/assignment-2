# Assignment 2

For Assignment 2 we were to develop an API that aggregates information from our Gitlab deployment, i.e., https://git.gvk.idi.ntnu.no.  
We were to develop an API for direct invocation, in addition to develop an interface for the registration of Webhooks for invocation upon certain events.  
Eventually the service were deployed on OpenStack.   

In the requirements of the assignment, we were to implement two out of three main endpoints, in addition to status endpoint and the webhook endpoint.  
For my assignment I chose the commits and language endpoint.   

Languages takes a post request for payload with the body format [repositoryId, repositoryId,...]. This means when testing, use int.   
Further the payload does not handle security token.  

Webhooks does only send time of endpoint and limit from user.  
For security reasons, authentication token is not sent.   

When using the endpoints, use the following syntax:     
For language:  
/ gives you the top 5 languages used over all repositories.   
If you want to send a payload to select only certain projects you can send a POST request with format [repositoryId, repositoryId,...]. Not using numbers will not work. If you want to use a private token, just add ?private_token={Your private token}. A faulty code will throw an error.  
If you want to use a custom limit, just add a ?limit={wanted limit} after languages.   
If you wish to use both limit and authentication, it must be in the correct order.   
Example: http://localhost:8080/repocheck/v1/languages?limit={Wanted limit}&private_token={Your private token}  

For commits:   
/ gives you the top 5 repository with the most commits.  
If using a private_token add ?private_token={Your private token}. A faulty code will trow an error.   
If using a custom limit, add ?limit={wanted limit} after commits. 

For status: 
Here I only have the landing page. 

For webhooks:  
/ gives you a default page listing all webhooks that is currently in use.  
By using /webhookID in the URL you can look up webhooks stored in the database.  
You can delete a webhook using /webhookID in the URL and by using a DELETE request.   
You can save a new webhook using a POST request and a body following the template:  
{  
  "event": "(commits|languages|issues|status)",   
  "url": "your chosen URL for receiving notifications on."   
}  
Failed to chose either of these three events will trow an error.   

There are no test made for this assignment.  

 
  

